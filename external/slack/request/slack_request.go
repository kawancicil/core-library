package request

type SlackRequest struct {
	Channel   string `json:"channel"`
	Username  string `json:"username"`
	Text      string `json:"text"`
	IconEmoji string `json:"icon_emoji"`
}

type SlackLogPayloadRequest struct {
	Endpoint string `json:"endpoint"`
	Request  string `json:"request"`
	Response string `json:"response"`
}

type SlackRequestV2 struct {
	EnvironmentName string `json:"environment_name"`
	ServiceName     string `json:"service_name"`
	ErrorDetail     string `json:"error_detail"`
	Flow            string `json:"flow"`
	WebHookUrl      string `json:"web_hook_url"`
}

func NewSlackRequestV2(err, flow, environmentName, webhookUrl, serviceName string) (req SlackRequestV2) {
	req.EnvironmentName = environmentName
	req.ErrorDetail = err
	req.Flow = flow
	req.WebHookUrl = webhookUrl
	req.ServiceName = serviceName
	return
}
